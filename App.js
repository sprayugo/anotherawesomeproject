/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Button,
  Image,
  View,
  Text,
  StatusBar,
  Alert,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Image
          source={{uri: "https://asset.kompas.com/crops/anH8-9BBERzxycwnUgPN3z6rkPs=/1x0:1280x853/750x500/data/photo/2020/02/06/5e3b03f117be3.jpeg"}}
          style={{width: 420, height: 200}}
        />
        <ScrollView>
          <Text>
            Xpander Cross hadir lebih tangguh dengan desain Dynamic Shield khas Mitsubishi Motors yang menggambarkan keseimbangan antar kedinamisan dan power.{'\n'}
            Dirancang dengan ground cleareance setinggi 225mm, Xpander Cross memberikan kenyamanan berkendara terbaik di kelasnya dan mampu melalui berbagai medan.
          </Text>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
